package com.android.safeter;

public class MenuItem {
	
	private String Marca;
	private int Id;
	private int cantidad;
	private double Precio;
	
	public MenuItem(){
		
	}
	
	public MenuItem(String Marca, Double Precio,int cantidad){
		this.Marca=Marca;
		this.Precio=Precio;
		this.cantidad=cantidad;
	}
	
	public String toString(){
		return this.Id+": "+this.Marca+" :"+this.Precio+" :"+this.cantidad;
	}

	public String getMarca() {
		return Marca;
	}

	public void setMarca(String marca) {
		Marca = marca;
	}

	public double getPrecio() {
		return Precio;
	}

	public void setPrecio(double precio) {
		Precio = precio;
	}

	public int getId() {
		return Id;
	}

	public void setId(int Id) {
		this.Id = Id;
	}

	public int getCantidad() {
		return this.cantidad;
	}

	public void setCantidad(int cantidad) {
		this.cantidad = cantidad;
	}

}
