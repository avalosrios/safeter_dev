package com.android.safeter;

import java.util.ArrayList;
import java.util.List;


import com.google.gson.Gson;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.Toast;

public class MenuActivity extends Activity {

	private ListView list;
	private int menuId=1;
	private String jsonStr;

	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) 
	{
		super.onCreate(savedInstanceState);
		//Setting view
		setContentView(R.layout.menu_bebidas);
		setTitle("SafeterMenu");
		HttpRequest request = new HttpRequest();
		
		//If the activity has been opened before
		// Restore preferences
		SharedPreferences settings = getSharedPreferences(this.menuId+"", 0);
		this.jsonStr = settings.getString(this.menuId+"", "");

		//Obtenemos los valores en forma de arrego de string
		String [] values = this.getMenu(this.menuId+"", jsonStr);

		
		
		//TODO Guardar los values para que cuando regrese no tenga que cargar todo de nuevo

		List <MenuList> menuList = new ArrayList<MenuList>();
		//Creo la lista de elementos
		String ext = ".png";
		for (int i = 0; i<values.length; i++){
			String name = values[i];
			String item = values[i];
			//String img = (i%2==0)?"icon_2.png":"icon_1.png";
			String img = values[i]+ext;
			MenuList listItem = new MenuList(name,item,img);
			menuList.add(listItem);
		}

		// Create a customized ArrayAdapter
		MenuListAdapter adapter = new MenuListAdapter(
				getApplicationContext(), R.layout.list_view_menu, menuList);

		// Get reference to ListView holder
		ListView lv = (ListView) this.findViewById(R.id.listView1);

		// Set the ListView adapter
		lv.setAdapter(adapter);

		/**ESTO ES PARA EL CLICK*/
		lv.setOnItemClickListener(new OnItemClickListener() {
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
				// TODO Auto-generated method stub
				Object o = parent.getAdapter().getItem(position);
				String bebida = o.toString();
				// When clicked, show a toast with the TextView text
				Toast.makeText(getApplicationContext(), bebida,
						Toast.LENGTH_SHORT).show();
				//Creamos y cargamos la siguiente intent
				Intent intent = new Intent(getApplicationContext(),SubMenuActivity.class);
				//pasamos la variable de familia
				String familia = bebida;
				intent.putExtra("familia", familia);
				
				SharedPreferences settings = getSharedPreferences("ht"+familia, 0);
				System.out.println("Tenemos algo en esa familia?? "+settings.getString(familia, "NO hay NAAA!!"));
				startActivity(intent);
			}
		});
	}
		
	
	/**
	 * En dado caso de que se guarde algo lo carga
	 * si no hay nada lo vuelve a pedir en el servidor
	 * @param menuId
	 * @param jsonStr
	 * @return
	 */
	private String [] getMenu(String menuId, String jsonStr){
		String [] menu= null;
		if (jsonStr.length()>0){
			Gson gson = new Gson();
			menu = gson.fromJson(jsonStr, String[].class);
		}else{
			//peticion al servidor de cosas de la lista
			HttpRequest request = new HttpRequest();
			menu = request.getFamilias("familias="+menuId,"menuController.php");
		}
		this.saveData(menu);
		System.out.println("Termine de colocar las cosas");
		return menu;
	}

	private void saveData(String [] data){
		Gson gson = new Gson();
		this.jsonStr=gson.toJson(data);
		System.out.println("DAta se ha guardado "+this.jsonStr);
	}
	/**
	 * 
	 * @param v
	 */
	public void backHandler(View v){
		// Cuando dan click a boton back de la interfaz
		Intent myIntent = new Intent(MenuActivity.this,SafeterActivity.class);
		System.out.println("boton back overrideed!!");
		startActivity(myIntent);
	}

	//override del boton back
	public void onBackPressed() {
		// Cuando dan click a boton back del celular
		Intent myIntent = new Intent(MenuActivity.this,SafeterActivity.class);
		startActivity(myIntent);
	}
	
	@Override
	protected void onStop(){
		super.onStop();
		System.out.println("Haciendo uso del stop method y guardando info segun esto");
		// All objects are from android.context.Context
		SharedPreferences settings = getSharedPreferences(this.menuId+"", 0);
		SharedPreferences.Editor editor = settings.edit();
		editor.putString(this.menuId+"", this.jsonStr);
		System.out.println("STOP SAVE!! "+settings.getString(this.menuId+"", "nada =("));
		// Commit the edits!
		editor.commit();
	}
}