package com.android.safeter;


import java.util.ArrayList;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class login extends Activity {
	
	//Campos del login
	EditText un,pw;
	//boton acceso
	Button ok;
	
	//objeto httprequest
	HttpRequest request = new HttpRequest();
	
    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login);
        
        un=(EditText)findViewById(R.id.recuadro_email);
        pw=(EditText)findViewById(R.id.recuadro_password);
        ok=(Button)findViewById(R.id.boton_login);
          		 

        ok.setOnClickListener(new View.OnClickListener() {
        	
        	 
        	
        	            @Override
        	
        	            public void onClick(View v) {
        	
        	                // TODO Auto-generated method stub
        	
        	 
        	
        	                ArrayList<NameValuePair> postParameters = new ArrayList<NameValuePair>();
        	
        	                postParameters.add(new BasicNameValuePair("mail", un.getText().toString()));
        	
        	                postParameters.add(new BasicNameValuePair("pwd", pw.getText().toString()));
        	
        	 
        	
        	                String response = null;
        	
        	                try {
        	
        	                    response = request.executeHttpPost("/loginController.php", postParameters);
        	
        	                    String res=response.toString();
        	
        	                    res= res.replaceAll("\\s+","");
        	
        	                    if(res.equals("0")){
        	                  		
        	            		      System.out.println("sirvio");
        	            		
        	            		      }else{
        	            		    	   
        	            		    	  
        	            		      System.out.println("no sirve");
        	            		      }
        	                }catch (Exception e) {
        	
        	                    un.setText(e.toString());
        	
        	                }
        	
        	 
        	
        	            }
        	
        	        });


        TextView registerScreen = (TextView) findViewById(R.id.link_registrarse);
        registerScreen.setOnClickListener(new View.OnClickListener() {
        	 
            public void onClick(View v) {
                // Switching to Register screen
                Intent i = new Intent(getApplicationContext(), registro.class);
                startActivity(i);
            }
        });
    }
}