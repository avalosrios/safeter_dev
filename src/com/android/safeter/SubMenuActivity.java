package com.android.safeter;
//TODO Cambiar lo de las keys para que guarde si es copa o botella, usan mismo id
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Set;
import com.android.safeter.MenuItem;
import com.google.gson.Gson;
import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TableRow;
import android.widget.TextView;

public class SubMenuActivity extends Activity implements OnCustomClickListener{
	private ListView submenu;
	private HashMap<String,MenuItem> ht= new HashMap<String,MenuItem>();
	private String familia;
	private String jsonHt;
	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) 
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.menu_especializado_bebidas);	

		//Get variables and set
		Bundle extras = getIntent().getExtras();
		this.familia = "";
		if(extras !=null) {
			this.familia = extras.getString("familia");
		}

		System.out.println("Familia "+this.familia);

		/*
		//Set title
		TextView titulo = new TextView(this);
		titulo = (TextView)findViewById(R.id.texto_encabezado);
		titulo.setText(this.familia);
*/
		String htJson = "";

		//If the activity has been opened before
		// Restore preferences
		SharedPreferences settings = getSharedPreferences("ht"+this.familia, 0);
		htJson = settings.getString(this.familia, "");

		//Retrieve saved data if any, when a call or something happens that makes close
		if(savedInstanceState != null){
			if (savedInstanceState.getString("ht"+this.familia).length()>0){
				System.out.println("Parece que guardamos algo en key "+this.familia);
				htJson = savedInstanceState.getString("ht"+this.familia);
			}			
		}
		//hacer la peticion al servidor y obtener las bebidas
		System.out.println("Esto es lo que hay"+htJson);

		this.submenu = (ListView)findViewById(R.id.items);
		//Obtenemos el listado que se ve a usar para llenar la list view
		List menuList = this.getBebidas(familia,htJson);
		SubmenuRowAdapter adapter = new SubmenuRowAdapter(
				getApplicationContext(), R.layout.custom_submenu_list, menuList, this);
		this.submenu.setAdapter(adapter);		
	}

	public void readModifiedData(SubmenuRowAdapter adapter){
		int size = adapter.getCount();
		for (int i=0; i<size; i++){
			System.out.println("Elemento "+adapter.getItem(i));
		}
		
	}
	//metodo actualizado con id
	private ArrayList<MenuItem> getBebidas(String familia, String jsonObj){
		ArrayList<MenuItem> list = new ArrayList<MenuItem>();
		MenuItem [] submenu = null;
		System.out.println("0000000 "+jsonObj);
		if(jsonObj.length()>0){
			Gson gson = new Gson();
			submenu = gson.fromJson(jsonObj, MenuItem[].class);
		}else{
			//hacemos http request
			HttpRequest request = new HttpRequest();
			submenu = request.getBebidas("marcas="+familia, "menuController.php");
		}
		for (int j=0; j<submenu.length; j++){
			list.add(submenu[j]);
			///Esto nesta feo
			ht.put(submenu[j].getMarca(),submenu[j]);
		}
		System.out.println("Termine de colocar las cosas");
		this.transformToJson();
		return list;
	}

	public void backHandler(View v){
		// Cuando dan click a boton back de la interfaz
		Intent myIntent = new Intent(SubMenuActivity.this,MenuActivity.class);
		System.out.println("Back Button");
		this.transformToJson();
		startActivity(myIntent);
	}

	//override del boton back
	public void onBackPressed() {
		// Cuando dan click a boton back del celular
		Intent myIntent = new Intent(SubMenuActivity.this,MenuActivity.class);
		System.out.println("boton back overrideed!!");
		this.transformToJson();
		System.out.println("A JSON"+this.jsonHt);
		myIntent.putExtra("ht", this.jsonHt);
		startActivity(myIntent);
	}

	/**
	 * Save whatever is in hashtable and parse to json
	 */
	private void transformToJson(){
		Gson gson = new Gson();
		MenuItem[]  response=new MenuItem[this.ht.size()];
		Set<String> keys = this.ht.keySet();  
		int i=0;
		for (String key : keys) {
			System.out.println(keys);
			response[i]=this.ht.get(key);
			i++;
		} 
		this.jsonHt = gson.toJson(response);
	}

	/**
	 * Method for saving data
	 */
	/*protected void onSaveInstanceState(Bundle savedInstanceState) {
		this.transformToJson();
		savedInstanceState.putString("ht"+this.familia, this.jsonHt);
		System.out.println("Saving "+"key "+this.familia+" Data "+ this.jsonHt);
		System.out.println("Saved instance has "+savedInstanceState.getString("ht"+this.familia));
		super.onSaveInstanceState(savedInstanceState);
	}*/


	@Override
	protected void onStop(){
		super.onStop();
		System.out.println("Haciendo uso del stop method y guardando info segun esto");
		// All objects are from android.context.Context
		SharedPreferences settings = getSharedPreferences("ht"+this.familia, 0);
		SharedPreferences.Editor editor = settings.edit();
		editor.putString(this.familia, this.jsonHt);
		System.out.println("STOP SAVE!! "+settings.getString("ht"+this.familia, "nada =("));
		// Commit the edits!
		editor.commit();
	}

	@Override
	public void OnCustomClickAdd(View view, String c) {
		// TODO Auto-generated method stub
		System.out.println("Click en sumar");
		//Obtenemos el row 
		View parent = (View) view.getParent();
		System.out.println(parent.getTouchables());
		TableRow vwParentRow = (TableRow)parent.getParent();
		System.out.println("Padre tiene hijos "+vwParentRow.getChildCount());
		//Sacamos el primer hijo que es el que tiene el nombre de la bebida
		TextView bebida = (TextView)vwParentRow.getChildAt(0);
		//Lo hacemos una llave para obtener los elementos del hash
		String key = bebida.getText().toString();
		System.out.println("key "+key );
		int count = this.ht.get(key).getCantidad()+1;
		System.out.println(count+"");
		//actualizando el contador en la list view
		TextView child = (TextView)((ViewGroup) parent).getChildAt(1);
		child.setText(count+"");
		//Cambiar el valor en la hash
		this.ht.get(key).setCantidad(count);
	}

	@Override
	public void OnCustomClickLess(View view, String c) {
		// TODO Auto-generated method stub
		System.out.println("Click en restar");
		//Obtenemos el row 
		View parent = (View) view.getParent();
		System.out.println(parent.getTouchables());
		TableRow vwParentRow = (TableRow)parent.getParent();
		//Sacamos el primer hijo que es el que tiene el nombre de la bebida
		TextView bebida = (TextView)vwParentRow.getChildAt(0);
		//Lo hacemos una llave para obtener los elementos del hash
		String key = bebida.getText().toString();
		int count = this.ht.get(key).getCantidad();
		count = count>0? count-1: count;
		System.out.println(count+"");
		TextView child = (TextView)((ViewGroup) parent).getChildAt(1);
		child.setText(count+"");
		//Cambiar el valor en la hash
		this.ht.get(key).setCantidad(count);
	}
}
