package com.android.safeter;

import java.util.ArrayList;
import android.widget.Button;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.view.View.OnClickListener;

public class SubmenuRowAdapter extends ArrayAdapter {

	private HashMap<String,MenuItem> ht= new HashMap<String,MenuItem>();
	private Context context;
	private List<MenuItem> items;
	private OnCustomClickListener callback;
	
	public SubmenuRowAdapter(Context context, int resource,
			List objects,OnCustomClickListener callback) {
		super(context, resource, objects);
		this.context = context;
		this.items = objects;
		this.callback=callback;
		// TODO Auto-generated constructor stub
	}

	public MenuItem getItem(int index){
		return this.items.get(index);
	}

	public int count(){
		return this.items.size();
	}

	public View getView(final int position, View convertView, ViewGroup parent) {
		SubMenuViewHolder holder;
		if (convertView == null) {
			LayoutInflater inflater = (LayoutInflater) this.getContext()
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			convertView = inflater.inflate(R.layout.custom_submenu_list,
					parent, false);
			holder = new SubMenuViewHolder();
			holder.bebidaEspecifica = (TextView) convertView.findViewById(R.id.bebidaEspecifica);
			holder.precio = (TextView) convertView.findViewById(R.id.precio);
			holder.layout = (RelativeLayout)convertView.findViewById(R.id.relativeLayoutSubmenu);
			holder.cantidad = (TextView) holder.layout.getChildAt(1);
			holder.lessButton = (Button) convertView.findViewById(R.id.lessButton);
			holder.sumButton = (Button) convertView.findViewById(R.id.sumButton);						
			convertView.setTag(holder);
		} else {
			holder = (SubMenuViewHolder) convertView.getTag();
		}
		
		//Aqui ya se establecen que contenido tendra cada cosa
		MenuItem menuItem = this.getItem(position);
		holder.bebidaEspecifica.setText(menuItem.getMarca());
		holder.precio.setText(menuItem.getPrecio()+"");
		holder.cantidad.setText(menuItem.getCantidad()+"");
		
		//Se ponen los listeners para el click
		holder.sumButton.setOnClickListener(new CustomOnClickListener (callback,"+"));
		holder.lessButton.setOnClickListener(new CustomOnClickListener(callback,"-"));
		
		return convertView;
	}
}
