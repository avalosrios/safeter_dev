package com.android.safeter;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class OtraActActivity extends Activity {
    /** Called when the activity is first created. */
    
   
    String numBotellas="";
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main2);
        
       
        
        final Button button = (Button) findViewById(R.id.button);
        button.setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
                // Perform action on clicks
         	   Intent myIntent = new Intent(); 
 				myIntent.setClassName("mx.safeter", "mx.safeter.SafeterActivity"); 
 				startActivity(myIntent); 
            }
        });
        
        
        
        final Button buttonDialog = (Button) findViewById(R.id.buttonDialog);
        buttonDialog.setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
            	final EditText input = new EditText(OtraActActivity.this);
                // Perform action on clicks
            	new AlertDialog.Builder(OtraActActivity.this)
                .setTitle("Update Status")
                .setMessage("Cuantas botellas")
                .setView(input)
                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                     public void onClick(DialogInterface dialog, int whichButton) {
                    	 numBotellas = input.getText().toString();
                         // deal with the editable
                     }
                })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                     public void onClick(DialogInterface dialog, int whichButton) {
                            // Do nothing.
                     }
                }).show();
            }
        });
        
  /*      final Button buttonToast = (Button) findViewById(R.id.buttonToast);
        buttonToast.setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
                // Perform action on clicks
            	 Context context = getApplicationContext();
                 CharSequence text = "Hello toast!"+ numBotellas;
                 int duration = Toast.LENGTH_SHORT;

                 Toast toast = Toast.makeText(context, text, duration);
                 toast.show();
            }
        });*/
       
        
    }
   
}