package com.android.safeter;

import java.util.List;

import android.view.View;
import android.view.View.OnClickListener;

public class CustomOnClickListener implements OnClickListener{
	//private List<MenuItem> items;
	private OnCustomClickListener callback;
	private String c;
	public CustomOnClickListener(OnCustomClickListener callback, String c){
		//this.items = items;
		this.callback = callback;
		this.c = c;
	}
	
	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		if (this.c.equals("+"))
			this.callback.OnCustomClickAdd(v, c);
		else
			this.callback.OnCustomClickLess(v, c);
	}

}
