package com.android.safeter;

import android.app.Activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;


public class SafeterActivity extends Activity {
   /** Called when the activity is first created. */
   @Override
   public void onCreate(Bundle savedInstanceState) {
       super.onCreate(savedInstanceState);
       setContentView(R.layout.menu_principal);
      
       
       final Button button = (Button) findViewById(R.id.boton_ordenar_bebidas);
       button.setOnClickListener(new OnClickListener() {
           public void onClick(View v) {
               // Perform action on clicks
        	   Intent myIntent = new Intent(); 
				myIntent.setClassName("com.android.safeter", "com.android.safeter.MenuActivity"); 
				startActivity(myIntent); 
           }
       });
       
       final Button button2 = (Button) findViewById(R.id.boton_checar_cuenta);
       button2.setOnClickListener(new OnClickListener() {
           public void onClick(View v) {
               // Perform action on clicks
        	   Intent myIntent = new Intent(); 
				myIntent.setClassName("com.android.safeter", "com.android.safeter.login"); 
				startActivity(myIntent);
				
				}
       });
       
       final Button button3 = (Button) findViewById(R.id.boton_pagar);
       button3.setOnClickListener(new OnClickListener() {
           public void onClick(View v) {
               // Perform action on clicks
        	   Intent myIntent = new Intent(); 
				myIntent.setClassName("com.android.safeter", "com.android.safeter.demoOrdenar"); 
				startActivity(myIntent);            }
       });
   }
  
		
   
}