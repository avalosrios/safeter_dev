package com.android.safeter;

public class MenuList{
	private String name;
	private String item;
	private String imageId;
	public MenuList()
	{
		
	}
	
	public MenuList(String name, String item, String imageId)
	{
		this.setName(name);
		this.item=item;
		this.setImageId(imageId);
	}
	
	public String toString(){
		return this.getName();
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getImageId() {
		return imageId;
	}

	public void setImageId(String imageId) {
		this.imageId = imageId;
	}
	
}
