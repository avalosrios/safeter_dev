package com.android.safeter;



import java.util.ArrayList;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

public class registro extends Activity {
	
	//Campos del login
		EditText nombre,email,pwd,fecha;
	//boton acceso
		Button ok;
	//Spinner
		Spinner sexo;
		
	//objeto httprequest
	HttpRequest request = new HttpRequest();
		
	@Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // Set View to register.xml
        setContentView(R.layout.register);
        
        nombre=(EditText)findViewById(R.id.relleno_nombre);
        email=(EditText)findViewById(R.id.relleno_email);
        fecha=(EditText)findViewById(R.id.relleno_fecha);
        pwd=(EditText)findViewById(R.id.relleno_password);
        ok=(Button)findViewById(R.id.boton_registro);
        sexo=(Spinner)findViewById(R.id.opcion);
        
        ok.setOnClickListener(new View.OnClickListener() {
        	
       	 
        	
            @Override

            public void onClick(View v) {

                // TODO Auto-generated method stub

 

                ArrayList<NameValuePair> postParameters = new ArrayList<NameValuePair>();
                
                postParameters.add(new BasicNameValuePair("mail", email.getText().toString()));
                postParameters.add(new BasicNameValuePair("nombre", nombre.getText().toString()));
                postParameters.add(new BasicNameValuePair("genero", sexo.getSelectedItem().toString()));
                postParameters.add(new BasicNameValuePair("pwd", pwd.getText().toString()));
                postParameters.add(new BasicNameValuePair("pwd1", pwd.getText().toString()));
                
                
                System.out.println(postParameters.toString());

                String response = null;

                try {

                    response = request.executeHttpPost("/createAccountController.php", postParameters);

                    String res=response.toString();

                    res= res.replaceAll("\\s+","");

                    if(res.equals("0")){
                  		
            		      System.out.println("sirvio");
            		
            		      }else{
            		    	   
            		    	  System.out.println(res.toString());  
            		      System.out.println("no sirve");
            		      }
                }catch (Exception e) {

                   // un.setText(e.toString());

                }

 

            }

        });
        
        
        TextView loginScreen = (TextView) findViewById(R.id.link_login);
        loginScreen.setOnClickListener(new View.OnClickListener() {
        	 
            public void onClick(View arg0) {
                                // Closing registration screen
                // Switching to Login Screen/closing register screen
                finish();
            }
        });
    }
}

