package com.android.safeter;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.HttpParams;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.conn.params.ConnManagerParams;
import org.apache.http.params.HttpConnectionParams;
import android.util.Log;
import com.google.gson.Gson;


public class HttpRequest 
{
    String url;
    

    
    //donde guardamos los datos que mandamos del usuario
    List<NameValuePair> nameValuePair;
   
	
    public HttpRequest()
    {
    	this.url="http://guxon.com";
    	
    	
        
    }
    
    
    //metodo post para login
    public  String executeHttpPost(String url, ArrayList<NameValuePair> postParameters) throws Exception {
    	
    	        BufferedReader in = null;
    	
    	        try {
    	
    	            HttpClient client = new DefaultHttpClient();
    	
    	            HttpPost request = new HttpPost(this.url+url);
    	
    	            UrlEncodedFormEntity formEntity = new UrlEncodedFormEntity(postParameters);
    	
    	            request.setEntity(formEntity);
    	
    	            HttpResponse response = client.execute(request);
    	
    	            in = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
    	
    	 
    	
    	            StringBuffer sb = new StringBuffer("");
    	
    	            String line = "";
    	
    	            String NL = System.getProperty("line.separator");
    	
    	            while ((line = in.readLine()) != null) {
    	
    	                sb.append(line + NL);
    	
    	            }
    	
    	            in.close();
    	
    	 
    	
    	            String result = sb.toString();
    	
    	            return result;
    	
    	        } finally {
    	
    	            if (in != null) {
    	
    	                try {
    	
    	                    in.close();
    	
    	                } catch (IOException e) {
    	
    	                    e.printStackTrace();
    	
    	                }
    	
    	            }
    	
    	        }
    	
    	    }
    

    public MenuItem[] getBebidas(String request, String controller){
    	//Para parsear el json
    	Gson gson = new Gson();
    	MenuItem[]  response = null; //Aqui guardaremos la info
    	try 
    	{
            URL url = new URL(this.url+"/"+controller+"?"+request);
            System.out.println(url);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setDoOutput(true);
            connection.setRequestMethod("GET");                       
            if (connection.getResponseCode() == HttpURLConnection.HTTP_OK) 
            {
                // OK
            	System.out.println(connection.getResponseMessage());//Esto nomas imprime el OK de que si esta bien
            	BufferedReader in = new BufferedReader(new InputStreamReader(connection.getInputStream()));
            	String decodedString;
            	
            	//Leemos todo el output
            	
            	while ((decodedString = in.readLine()) != null) {
            		if (decodedString.trim().length()>0 && decodedString.startsWith("["))
            		{
            			response = gson.fromJson(decodedString, MenuItem[].class);
            			String a = gson.toJson(response);
            		}
            	    System.out.println(decodedString);           	    
            	}
            	in.close();
            } 
            else 
            {
            	System.out.println("Error");
                // Server returned HTTP error code.
            }

        } catch (MalformedURLException e) {
            // ...
        } catch (IOException e) {
            // ...
        }  	
    	
		return response;
    }
    
    public String[] getFamilias(String request, String controller)
    {
    	//Para parsear el json
    	Gson gson = new Gson();
    	String[]  response = null; //Aqui guardaremos la info
    	try 
    	{
            URL url = new URL(this.url+"/"+controller+"?"+request);
            System.out.println(url);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setDoOutput(true);
            connection.setRequestMethod("GET");            
            
            if (connection.getResponseCode() == HttpURLConnection.HTTP_OK) 
            {
                // OK
            	System.out.println(connection.getResponseMessage());//Esto nomas imprime el OK de que si esta bien
            	BufferedReader in = new BufferedReader(new InputStreamReader(connection.getInputStream()));
            	String decodedString;
            	
            	//Leemos todo el output
            	
            	while ((decodedString = in.readLine()) != null) {
            		if (decodedString.trim().length()>0 && decodedString.startsWith("["))
            		{
            			response = gson.fromJson(decodedString, String[].class);
            			String r = gson.toJson(response);
            		}
            	    System.out.println(decodedString);           	    
            	}
            	in.close();
            } 
            else 
            {
            	System.out.println("Error");
                // Server returned HTTP error code.
            }

        } catch (MalformedURLException e) {
            // ...
        } catch (IOException e) {
        	System.out.println(e);
            // ...
        }  	
    	
		return response;
    }
    
}
