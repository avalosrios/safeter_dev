package com.android.safeter;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

public class SafeterSplash extends Activity {
	protected boolean _active = true;
	protected int _splashTime = 1250; // time to display the splash screen in ms


	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {
	    super.onCreate(savedInstanceState);
	    setContentView(R.layout.main);
	 
	    // thread for displaying the SplashScreen
	    Thread splashTread = new Thread() {
	        @Override
	        public void run() {
	            try {
	                int waited = 0;
	                while(_active && (waited < _splashTime)) {
	                    sleep(100);
	                    if(_active) {
	                        waited += 100;
	                    }
	                }
	            } catch(InterruptedException e) {
	                // do nothing
	            } finally {
	                finish();
	                Intent intent = new Intent(getApplicationContext(),SafeterActivity.class);
	 				startActivity(intent);	               
	            }
	        }
	    };
	    splashTread.start();
	}
}