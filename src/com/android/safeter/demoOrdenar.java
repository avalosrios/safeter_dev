package com.android.safeter;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;




public class demoOrdenar extends Activity {
    /** Called when the activity is first created. */
    
    
    
    int chelas=0;
    int chescos=0;
    int lobas=0;
    int agua=0;
    
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.ordenar);
        
        
        final Button buttonChelasMa = (Button) findViewById(R.id.buttonChelaMa);
        final Button buttonChelasMe = (Button) findViewById(R.id.buttonChelaMe);
        final Button buttonChescosMa = (Button) findViewById(R.id.buttonChescosMa);
        final Button buttonChescosMe = (Button) findViewById(R.id.buttonChescosMe);
        final Button buttonAguaMa = (Button) findViewById(R.id.buttonAguaMa);
        final Button buttonAguaMe = (Button) findViewById(R.id.buttonAguaMe);
        final Button buttonLobasMa = (Button) findViewById(R.id.buttonLobasMa);
        final Button buttonLobasMe = (Button) findViewById(R.id.buttonLobasMe);
        
        final Button buttonSubmit = (Button) findViewById(R.id.buttonSubmit);
        
        final EditText editTextLobas = (EditText)findViewById(R.id.editTextLobas);
        final EditText editTextChescos = (EditText)findViewById(R.id.editTextChescos);
        final EditText editTextAgua = (EditText)findViewById(R.id.editTextAgua);
        final EditText editTextChelas = (EditText)findViewById(R.id.editTextChelas);
        
        
        
        buttonSubmit.setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
                // Perform action on clicks
            	Intent myIntent = new Intent(demoOrdenar.this,orden.class); 
				//myIntent.setClassName("mx.safeter", "mx.safeter.OtraActActivity"); 
				
				myIntent.putExtra("chelas", chelas);
				myIntent.putExtra("lobas", lobas);
				myIntent.putExtra("chescos", chescos);
				myIntent.putExtra("agua", agua);
				
				startActivity(myIntent);
            }
        });
        
        buttonChelasMa.setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
                // Perform action on clicks
         	   chelas++;
         	  //Toast.makeText(demoOrdenar.this, Integer.toString(chelas), Toast.LENGTH_SHORT).show();
         	  editTextChelas.setText(Integer.toString(chelas));
            }
        });
        buttonChelasMe.setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
                // Perform action on clicks
         	   chelas--;
         	  editTextChelas.setText(Integer.toString(chelas));
            }
        });
        buttonChescosMa.setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
                // Perform action on clicks
         	  chescos++;
         	editTextChescos.setText(Integer.toString(chescos));
            }
        });
        buttonChescosMe.setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
                // Perform action on clicks
         	   chescos--;
         	  editTextChescos.setText(Integer.toString(chescos));
            }
        });
        buttonAguaMa.setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
                // Perform action on clicks
         	   agua++;
         	  editTextAgua.setText(Integer.toString(agua));
            }
        });
        buttonAguaMe.setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
                // Perform action on clicks
         	   agua--;
         	  editTextAgua.setText(Integer.toString(agua));
            }
        });
        buttonLobasMa.setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
                // Perform action on clicks
         	   lobas++;
         	  editTextLobas.setText(Integer.toString(lobas));
            }
        });
        buttonLobasMe.setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
                // Perform action on clicks
         	   lobas--;
         	  editTextLobas.setText(Integer.toString(lobas));
            }
        });
        
    }
   
}